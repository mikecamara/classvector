var annotated_dup =
[
    [ "Date", "class_date.html", "class_date" ],
    [ "Metadata", "class_metadata.html", "class_metadata" ],
    [ "MonthRecord", "class_month_record.html", "class_month_record" ],
    [ "RecordContainer", "class_record_container.html", "class_record_container" ],
    [ "Time", "class_time.html", "class_time" ],
    [ "Vector", "class_vector.html", "class_vector" ],
    [ "YearRecord", "class_year_record.html", "class_year_record" ]
];