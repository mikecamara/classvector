
/* 
 * File:   recordContainer.h
 * Author: mcmikecamara
 *
 * Created on April 19, 2016, 10:49 PM
 */

#ifndef RECORDCONTAINER_H
#define RECORDCONTAINER_H


//------------------------------------------------------------------------------

#include <iostream>
#include <string>  
#include "yearRecord.h"
#include "vector.h"


using namespace std;


//------------------------------------------------------------------------------
//

	/**
	 * @class RecordContainer
	 * @brief  Constructor, setter and getter methods for RecordContainer.
	 *
	 * The Constructor get 3 given parameters, Unit unit, string mark, 
         * Date date.
         *
	 *
	 *
	 * @author Mike Gomes
	 * @version 01
	 * @date 31/03/2015 Mike Gomes, Started
	 *
	 *
	 *
	 * @todo Check if is there something wrong with the class
	 *
	 * @bug Haven't used the class yet.
	 */

class RecordContainer 
{
    
public:
  
    /**
     * @brief  Constructor of RecordContainer class
     *
     * This function can be called to create Result objects and requires a 
     * \unit object,\mark int and \date Date
     * 
     *
     * @param  unit, mark and date
     * @return void
     */
    RecordContainer ();
    ~RecordContainer () {};
    
    RecordContainer(Vector<YearRecord> yearRecordList);
    
    void Clear ();
  
    
    /**
     * @brief  returns a unit result date
     *
     * This function can be called to get the date of the unit result
     *
     * @return string with unit date 
     */    
    int getCurrentYear();
   
    
protected:
    
private:
    
    Vector<YearRecord> m_yearRecordList;
};

#endif /* RECORDCONTAINER_H */

