
/* 
 * File:   yearRecord.h
 * Author: mcmikecamara
 *
 * Created on April 19, 2016, 10:33 PM
 */

#ifndef YEARRECORD_H
#define YEARRECORD_H

//------------------------------------------------------------------------------

#include <iostream>
#include <string>  
#include "date.h"
#include "time.h"
#include "monthRecord.h"
#include "vector.h"


using namespace std;


//------------------------------------------------------------------------------
//

	/**
	 * @class YearRecord
	 * @brief  Constructor, setter and getter methods for YearRecord.
	 *
	 * 
	 *
	 *
	 * @author Mike Gomes
	 * @version 01
	 * @date 31/03/2015 Mike Gomes, Started
	 *
	 *
	 *
	 *
	 */

class YearRecord 
{
    
public:
  
    /**
     * @brief  Constructor of YearRecord class
    
     * 
     *
     * @param  
     * @return void
     */
    YearRecord ();
    ~YearRecord () {};
    
    YearRecord(int year, Vector<MonthRecord> monthList);
        YearRecord(int year);

    
    void Clear ();
       
     
    
    
    /**
     * @brief  Set specified unit mark
     *   
     * 
     *
     * @param  int - mark
     * @return void
     *
     */
    void setYear(int year);
    
    
    
    
    /**
     * @brief  returns a mark
     *
     * This function can be called to get the mark of a unit
     *
     * @return int with a mark 
     */
    int getYear() const;
    
    int getCurrentMonth() const;
 
    
   
    
protected:
    
private:
    
    int m_year;   /// mark of a result
    Vector<MonthRecord> m_monthList;
};



#endif /* YEARRECORD_H */

