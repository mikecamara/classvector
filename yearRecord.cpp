
// yearRecord.cpp
//
// Version 01
// 30/03/2016 Mike Gomes
// Started
//---------------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Includes

#include <iostream>
#include "vector.h"
#include "metadata.h"
#include "yearRecord.h"

using namespace std;


//----------------------------------------------------------------------------
// Function implementations

// Constructor of the YearRecord object
YearRecord::YearRecord()
{   
    Clear();
    m_year = 0;
}


YearRecord::YearRecord(int year, Vector<MonthRecord> monthList)
{
    m_year = year;
    m_monthList = monthList;
   
}

//----------------------------------------------------------------------------
// Set 
void YearRecord::setYear(int year)
{
    m_year = year;
}

//----------------------------------------------------------------------------
// Retrieves 
int YearRecord::getYear() const
{
    return m_year;
}


//int YearRecord::getCurrentMonth() const
//{
//    return m_monthList;
//}

//--------------------------------------------------------------------

void YearRecord::Clear ()
{
    m_year = 0;
    
}
