Challenge assignment 1

Due:4 pm - Friday 22/April

You have to read the file “MetData_Mar01-2014-Mar01-2015-ALL.csv” 

The file has weather sensors data of a specific location and time

Provide a suitable menu with an exit option in your main program. 
Only command line. 
Do not use GUI interaction.

1. The maximum wind speed for a specified month and year. 2. Average wind speed for each month of a specified year. 3. Total solar radiation in kWh/m2 for each month of a specified year. 4. Average wind speed km/h and total solar radiation in kWh/m for each month of aspecified year. (print to a file called “WindandSolar.csv”)


Data Structures:Reuse the Date, Time and template Vector classes from the previous laboratory exercises. A template vector class, called Vector must be used and you must write your own minimal and complete template Vector class to store data in a linear structure. For the purposes of the assignment, a vector class is a dynamic array encapsulated in a class. Access to the private array is through the vector’s public methods.