
// monthRecord.cpp
//
//
// Version 01
// 30/03/2016 Mike Gomes
// Started
//---------------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Includes

#include <iostream>
#include "vector.h"
#include "metadata.h"
#include "monthRecord.h"

using namespace std;



//----------------------------------------------------------------------------
// Function implementations

// Constructor of the MonthRecord object
MonthRecord::MonthRecord()
{   
    Clear();
    m_month = 0;
}


MonthRecord::MonthRecord(int month, Vector<Metadata> metadataList)
{
    m_month = month;
    m_metadataList = metadataList;
    
   
}

//----------------------------------------------------------------------------
// Set
void MonthRecord::setMonth(int month)
{
    m_month = month;
}

//----------------------------------------------------------------------------
// Retrieves 
int MonthRecord::getMonth() const
{
    return m_month;
}

//--------------------------------------------------------------------

void MonthRecord::Clear ()
{
    m_month = 0;
    
}
