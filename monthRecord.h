

/* 
 * File:   monthRecord.h
 * Author: mcmikecamara
 *
 * Created on April 19, 2016, 10:43 PM
 */

#ifndef MONTHRECORD_H
#define MONTHRECORD_H



//------------------------------------------------------------------------------

#include <iostream>
#include <string>  
#include "date.h"
#include "time.h"
#include "vector.h"

using namespace std;


//------------------------------------------------------------------------------
//

	/**
	 * @class MonthRecord
	 * @brief  Constructor, setter and getter methods for MonthRecord.
	 *
	 * The Constructor get 3 given parameters, Unit unit, string mark, 
         * Date date.
         *
	 *
	 *
	 * @author Mike Gomes
	 * @version 01
	 * @date 31/03/2015 Mike Gomes, Started
	 *
	 *
	 *
	 * @todo Check if is there something wrong with the class
	 *
	 * @bug Haven't used the class yet.
	 */

class MonthRecord 
{
    
public:
  
    /**
     * @brief  Constructor of MonthRecord class
     *
     * This function can be called to create Result objects and requires a 
     * \unit object,\mark int and \date Date
     * 
     *
     * @param  unit, mark and date
     * @return void
     */
    MonthRecord ();
    ~MonthRecord () {};
    
    MonthRecord(int month, Vector<Metadata> metadataList);
    
    void Clear ();
       
     
   
    /**
     * @brief  Set specified unit mark
     *   
     * 
     *
     * @param  int - mark
     * @return void
     *
     */
    void setMonth(int month);
    
   
    
    /**
     * @brief  returns a mark
     *
     * This function can be called to get the mark of a unit
     *
     * @return int with a mark 
     */
    
    int getMonth() const;
    

    
protected:
    
private:
    
    int m_month;   /// mark of a result
    Vector<Metadata> m_metadataList;
};



#endif /* MONTHRECORD_H */

