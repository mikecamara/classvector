
// recordContainer.cpp
//
//
// Version 01
// 30/03/2016 Mike Gomes
// Started
//---------------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Includes

#include <iostream>
#include "vector.h"
#include "metadata.h"
#include "recordContainer.h"

using namespace std;


//----------------------------------------------------------------------------
// Function implementations

// Constructor of the RecordContainer object
RecordContainer::RecordContainer()
{   
    Clear();
}


RecordContainer::RecordContainer(Vector<YearRecord> yearList)
{
    m_yearRecordList = yearList;
   
}


//RecordContainer::getCurrentYear()
//{
//    return m_yearRecordList;
//}

//--------------------------------------------------------------------

void RecordContainer::Clear ()
{
    
}
